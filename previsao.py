from flask import Flask
import requests

app = Flask(__name__)

@app.route("/")
def previsao():
    # link do open_weather: https://openweathermap.org/

    API_KEY = "7435c34b7b56559733cd3f7663ac162e"
    cidade = "Curitiba"
    link = f"https://api.openweathermap.org/data/2.5/weather?q={cidade}&appid={API_KEY}&lang=pt_br"

    requisicao = requests.get(link)
    requisicao_dic = requisicao.json()
    descricao = requisicao_dic['weather'][0]['description']
    temperatura = requisicao_dic['main']['temp'] - 273.15
    temperatura = format(temperatura, ".2f")  
    # print(descricao, f"{temperatura}ºC")
    return (
        "<h1>Previsão do tempo em: " + cidade + "</h1>"
        + "<b>Descrição: </b>" + descricao
        + "<br><b>Temperatura: </b>" + temperatura + "ºC"
    )

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True) 
    
